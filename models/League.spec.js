const User = require('./User');
const League = require('./League');
const expect = require('chai').expect;

describe('league', () => {
    it('should be invalid if name is empty', () => {
        let league = new League();
        league.validate((err) => {
            expect(err.errors.name).to.exist;
            done();
        });
    });

    it('should save', () => {
        let user = new User();
        user.name = "Marcelo";
        user.login = `macelo${new Date().getTime()}`;

        return new Promise((resolve, reject) => {
            user.save((err, docUser) => {
                if (err) {
                    reject(err);
                }
                resolve(docUser);
            })
        }).then((docUser) => {

            expect(docUser.name).to.exist;

            let league = new League();
            league.name = "Marcelo League";
            league.users.push(docUser);

            return new Promise((resolve, reject) => {
                league.save((err, docLeague) => {
                    if (err) {
                        reject(err);
                    }
                    resolve(docLeague);
                })
            });
        }).then((docLeague) => {
            expect(docLeague.name).to.exist;
        }).catch((err) => {
            console.log(err);
            expect(err).to.not.exist;
        });
    });
});
