const config = require('../config');
const utils = require('./utils');
const mongoose = config.mongoose;

const leagueSchema = new mongoose.Schema({
    name: {type: String, required: true},
    users: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
    created_at: Date,
    updated_at: Date
});

leagueSchema.pre('save', utils.beforeSave);

const League = mongoose.model('League', leagueSchema);

module.exports = League;
