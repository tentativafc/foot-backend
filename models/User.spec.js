const User = require('./User');
const expect = require('chai').expect;

describe('user', () => {
    it('should be invalid if name is empty', () => {
        let user = new User();
        user.validate((err) => {
            expect(err.errors.name).to.exist;
            done();
        });
    });

    it('should save', () => {
        let user = new User();
        user.name = "Marcelo";

        user.save((err, doc) => {
            expect(doc.name).to.exist;
            done();
        })
    });
});
