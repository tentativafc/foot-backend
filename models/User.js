const config = require('../config');
const utils = require('./utils');
const mongoose = config.mongoose;

const userSchema = new mongoose.Schema({
    name: {type: String, required: true},
    login: {type: String, unique: true},
    password: {type: String},
    created_at: Date,
    updated_at: Date
});

userSchema.pre('save', utils.beforeSave);

const User = mongoose.model('User', userSchema);

module.exports = User;
