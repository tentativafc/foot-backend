var config           = require('./config');
var path             = require('path');
var favicon          = require('static-favicon');
var logger           = require('morgan');
var cookieParser     = require('cookie-parser');
var bodyParser       = require('body-parser');
var express          = require('express');
var expressValidator = require('express-validator');
var jwt              = require('express-jwt');

// Routes
var routes           = require('./routes/index');
var LeagueController = require('./controllers/League');
var UserController   = require('./controllers/User');


/**
 * Create Express server.
 */
var app = express();
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(expressValidator());
app.use(logger('dev'));

app.get('/', function (req, res) {
    res.render('index');
  });

app.use(jwt({
    secret: config.secretKey,
    credentialsRequired: false,
    getToken: function fromHeaderOrQuerystring(req) {
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}));






  app.listen(3000, function () {
    console.log('Servidor rodando na porta 3000 !');
});
