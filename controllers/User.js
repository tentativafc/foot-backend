const User = require('../models/User');

/**
 * GET /api/users
 */
exports.getUsers = (req, res, next) => {
    User.find({}, function (err, docs) {
        if (err) {
            return next(err);
        }
        res.json(docs);
    });
};

/*
 * POST /api/users
 */
exports.postUsers = (req, res, next) => {
    let user = new User({
        name: req.body.name,
        login: req.body.login,
        password: req.body.password
    });

    user.save((err, doc) => {
        if (err) {
            next(err);
        }
        res.json(doc);
    })
};

