const League = require('../models/League');
const User = require('../models/User');

/**
 * GET /api/leagues
 */
exports.getLeagues = (req, res, next) => {
    League.find({})
        .populate('users', 'name login')
        .exec(function (err, docs) {
            if (err) {
                return next(err);
            }
            res.json(docs);
        });
};

/**
 * POST /api/leagues
 */
exports.postLeagues = (req, res, next) => {
    let usersId = req.body.usersId;
    let users = usersId.map((id) => new User({_id: id}));

    let league = new League({
        name: req.body.name,
        users: users
    });

    league.save((err, doc) => {
        if (err) {
            return next(err);
        }
        res.json(doc);
    })
};
