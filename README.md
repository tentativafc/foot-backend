#Foot Backend

## Comandos

1. Instala��o dos pacotes do projeto
```
npm install
```


2. Subir o aplicativo
```
npm start
```

3. Rodar os testes unit�rios
```
npm test
```

4. Instalar bibliotecas
```
npm install <NOME_DA_LIB> --save|--global|--saveDev
```

5. Instalar o mongodb e criar uma base chamada foot
