const env = process.env.NODE_ENV ? process.env.NODE_ENV : 'production';

const mongoose = require('mongoose');
//mongoose.connect(`mongodb://localhost/foot-${env}`);
mongoose.connect('mongodb://localhost:27017/foot');

const secretKey = process.env.SECRET_KEY ? process.env.SECRET_KEY : 'abcABC12355123';

module.exports = { env, mongoose, secretKey };



